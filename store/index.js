import Vue from 'vue'
import Vuex from 'vuex'
import footer_store from "./modules/footer_store.js"
var url_config = "";
import config_url from "@/common/config.js"
import api from "@/api/index.js"
import jpushIM from '@/common/im.js'
import imUtils from '@/common/imTools.js'
Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		videoList: [],
		baseUrl: config_url,
		user: {
			id: null
		},
		advert: {},
		subType: [],
		config: {},
		selectedCard: null,
		/* im-start */
		hasLogin: false,
		loginProvider: "", // 用户名或其他唯一值
		nickname: "", // 昵称
		signature: "", // 签名
		avatar: "/static/img/face.jpg", // 头像
		gender: "", // 性别，0未知，1男，2女
		openid: null,
		testvuex: false,
		graceIMMsgs: {},
		graceIMScTop: 0,
		receiveMessage: {}, // 接收新消息
		loginStateChanged: {}, // 登录状态变更
		sendMessageResponse: {}, // 消息发送结果回执
		receiveApplyJoinGroupApproval: [], // 入群通知列表
		friendInvitiaon: [], // 好友事件本地存储
		newFriendInvitiaon: 0, // 待处理的好友事件数量
		newReceiveApplyJoinGroupApproval: 0,//待处理的入群申请
		systemInfo: {}
	},
	mutations: {
		setSystemInfo(state, res) {
			state.systemInfo = res
		},
		setSelectCard(state, item) {
			state.selectedCard = item;
		},
		setVideoList(state, list) {
			state.videoList = list;
		},
		setUser(state, user) {
			uni.setStorageSync('user', user)
			state.user = user;
		},
		setSubType(state, subType) {
			state.subType = subType;
		},
		setAdvert(state, advert) {
			state.advert = advert;
		},
		setConfig(state, config) {
			state.config = config;
		},
		/* im-start */
		IMlogin(state, val) {
			state.hasLogin = true;
			state.loginProvider = val; // 用户名
		},
		IMlogout(state) {
			state.hasLogin = false
			state.loginProvider = ""
			state.openid = null
			state.avatar = "/static/img/face.jpg"
		},
		setOpenid(state, openid) {
			state.openid = openid
		},
		// 赋值登录名或唯一登录值
		setLoginProvider(state, val) {
			state.loginProvider = val
		},
		// 赋值昵称
		setNickname(state, val) {
			// console.log("setNickname：" + val);
			state.nickname = val
		},
		// 赋值性别
		setGender(state, val) {
			state.gender = val
		},
		// 赋值个人签名
		setSignature(state, val) {
			state.signature = val
		},
		// 赋值头像
		setAvatar(state, val) {
			state.avatar = val
		},
		// 赋值接收到的消息
		setReceiveMessage(state, param) {
			state.receiveMessage = param;
		},
		// 用户登录状态
		setLoginStateChanged(state, param) {
			state.loginStateChanged = param;
		},
		// 消息发送结果回执
		setSendMessageResponse(state, param) {
			state.sendMessageResponse = param;
		},
		// 新增好友消息事件
		addFriendInvitiaonChange(state, param) {
			state.newFriendInvitiaon++;
			var items = state.friendInvitiaon;
			items.unshift(param); // 保存在缓存中,此处应该配合API存在服务器上
			items = imUtils.combineObjectInList(items, "fromUsername"); // 数组去除重复，item为重复判定项
			state.friendInvitiaon = items; // 去重,重新赋值
		},
		setNewFriendInvitiaon(state, num) {
			state.newFriendInvitiaon = num; // 设置需要处理的好友数量
		},
		setFriendInvitiaon(state, param) {
			state.friendInvitiaon = param;
			state.newFriendInvitiaon = param.length;
		},
		addReceiveApplyJoinGroupApproval(state, param) {
			state.newReceiveApplyJoinGroupApproval++;
			var items = state.receiveApplyJoinGroupApproval;
			items.unshift(param); // 保存在缓存中,此处应该配合API存在服务器上
			state.receiveApplyJoinGroupApproval = items; // 重新赋值
		},
		setReceiveApplyJoinGroupApproval(state, param) {
			state.receiveApplyJoinGroupApproval = param;
			state.newReceiveApplyJoinGroupApproval = param.length;
		},
		/* im-end */
	},
	getters: {
		platform: state => {
			return state.systemInfo.platform
		},
		statusBarHeight: state => {
			return state.systemInfo.statusBarHeight
		}
	},
	actions: {
		selectCard(context, item) {
			context.commit('setSelectCard', item);
		},
		downloadAndInstall(context, url) {
			uni.showToast({
				title: '已开始下载，下载完成后将自动弹出安装程序。',
				duration: 3000,
				icon: "none"
			});
			var dtask = plus.downloader.createDownload(url, {}, function(d, status) {
				// 下载完成  
				if (status == 200) {
					plus.runtime.install(plus.io.convertLocalFileSystemURL(d.filename), {}, {}, function(error) {
						uni.showToast({
							title: '安装失败',
							mask: false,
							duration: 1500
						});
					})
				} else {
					uni.showToast({
						title: '下载失败',
						mask: false,
						duration: 1500
					});
				}
			});
			dtask.start();
		},
		update(context, isShow) {
			var name = plus.os.name;
			var params = { //升级检测数据  
				"appid": plus.runtime.appid,
				"version": plus.runtime.version,
				"os": name
			}
			api.update(params).then((res) => {
				console.log(res);
				if (res.statusCode == 200 && res.data.code === 0) {
					uni.showModal({ //提醒用户更新  
						title: "更新提示",
						content: res.data.data.note,
						success: (res1) => {
							if (res1.confirm) {
								//打开方式 1 直接下载 2 外部打开
								if (res.data.data.open_type == 1) {
									uni.showToast({
										title: '已开始下载，下载完成后将自动弹出安装程序。',
										duration: 3000,
										icon: "none"
									});
									var dtask = plus.downloader.createDownload(res.data.data.url, {}, function(d, status) {
										// 下载完成  
										if (status == 200) {
											plus.runtime.install(plus.io.convertLocalFileSystemURL(d.filename), {}, {}, function(error) {
												uni.showToast({
													title: '安装失败',
													mask: false,
													duration: 1500
												});
											})
										} else {
											uni.showToast({
												title: '更新失败',
												mask: false,
												duration: 1500
											});
										}
									});
									dtask.start();
								} else {
									plus.runtime.openURL(res.data.data.url);
								}

							}
						}
					})
				} else {
					if (isShow) {
						uni.showToast({
							title: '您已是最新版本，无需更新',
							icon: 'none'
						});
					}
				}
			})
		},
		getUserInfo(context) {
			var user = uni.getStorageSync("user");
			if (user) {
				var params = {
					token: user.token
				};
				api.userAuth(params).then((res) => {
					if (res.statusCode == 200) {
						if (res.data.code == 1) {
							context.commit('setUser', null)
						} else {
							context.commit('setUser', res.data.data)
						}
					}
				})
			} else {
				api.userInfo({}).then((res) => {
					if (res.statusCode == 200) {
						if (res.data.code == 1) {
							context.commit('setUser', null)
						} else {
							context.commit('setUser', res.data.data)
						}
					}
				})
			}
		},
		addVideoList(context, list) {
			for (let item of list) {
				context.state.videoList.push(item);
			}
		},
		getAdvert(context) {
			api.advertData({
				type: 1
			}).then((res) => {
				if (res.data.code == 1) {
					context.commit("setAdvert", {});
				} else {
					context.commit('setAdvert', res.data.data)
				}
			})
		},
		getConfig({ commit }) {
			api.config().then(({ data }) => {
				if (data.code === 0) {
					console.log()
					commit('setConfig', data.data)
				}
			})
		},
		/* im-start */
		// 提交聊天文字信息
		submitChatMsg: async function({
			commit,
			state,
			rootState
		}, params) {
			return await new Promise((resolve, reject) => {
				if (!state.hasLogin) {
					reject("请先登录")
				} else {
					switch (params.msgType) {
						case "text":
							// 文本消息
							delete(params["msgType"]);
							jpushIM.sendTextMessage(params, (res) => {
								if (res.errorCode == 0) {
									uni.showModal({
										title: '发送消息返回',
										content: '',
										showCancel: false,
										cancelText: '',
										confirmText: '',
										success: res => {},
										fail: () => {},
										complete: () => {}
									});
									resolve(res.data);
								} else {
									reject(res.errorMsg);
								}
							})
							break;
						case "image":
							// 图片消息
							// 将本地URL路径转换成绝对路径
							params.path = plus.io.convertLocalFileSystemURL(params.path);
							delete(params["msgType"]);
							jpushIM.sendImageMessage(params, (res) => {
								if (res.errorCode == 0) {
									resolve(res.data);
								} else {
									reject(res.errorMsg);
								}
							});
							break;
						case "voice":
							// 语音消息
							params.path = plus.io.convertLocalFileSystemURL(params.path);
							delete(params["msgType"]);
							// console.log("JMessagePlugin 发送语音消息:" + JSON.stringify(params));
							jpushIM.sendVoiceMessage(params, (res) => {
								if (res.errorCode == 0) {
									resolve(res.data);
								} else {
									reject(res.errorMsg);
								}
							});
							break;
						default:
							break;
					}

				}
			})
		},
		// 获取当前IM用户信息是否存在，存在则登录commit, state,rootState
		// 不用传参
		checkCurrentIMUser: async function({
			dispatch,
			commit
		}, params) {
			return await new Promise((resolve, reject) => {
				jpushIM.getMyInfo((res) => {
					if (res.errorCode == 0) {
						var data = res.data;
						commit('setLoginProvider', data.username);
						commit('setNickname', data.nickname);
						commit('setGender', data.gender);
						commit('setSignature', data.signature);
						commit('setAvatar', data.avatar);
						resolve(data);
					} else {
						reject(res.errorMsg);
					}
				})
			})
		},
		/**
		 * 更新我的头像
		 * @param {object} params = {
		 *  imgPath: string // 相册/拍照后的图片路径。
		 * }
		 * @param {function} callback = function ({'errorCode': '错误码', 'errorMsg': '错误信息',data:null}) {}
		 */
		updateMyAvatar: async function({
			commit,
			state,
			rootState
		}, params) {
			return await new Promise((resolve, reject) => {
				var origin = params.imgPath;
				if (origin) {
					// 将本地URL路径转换成平台绝对路径
					params.imgPath = plus.io.convertLocalFileSystemURL(params.imgPath);
				}
				jpushIM.updateMyAvatar(params, (res) => {
					if (res.errorCode == 0) {
						// var path = plus.io.convertAbsoluteFileSystem(params.imgPath);
						commit('setAvatar', origin);
						resolve(res);
					} else {
						reject(res.errorMsg);
					}
				})
			})
		},
		/* im-end */
	},
	modules: {
		footer_store
	}
})

export default store
