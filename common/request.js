import urlConfig from './config.js'

const request = {}
const headers = {}

request.globalRequest = (url, method, data = {}) => {
	return uni.request({
		url: urlConfig + url,
		sslVerify: false,
		method,
		data: data,
		dataType: 'json',
		withCredentials: true
	}).then(res => {
		console.log(url)
		console.log("返回数据", res[1].data);
		if (res[1].statusCode == 200) {
			return res[1]
		} else {
			throw res[1].data
		}
	}).catch(parmas => {
		switch (parmas.code) {
			case 401:
				uni.clearStorageSync()
				break
			default:
				return Promise.reject()
		}
	})
}

export default request
