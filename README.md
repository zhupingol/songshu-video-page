
# songshu-video-uniapp-YYC松鼠短视频源码-开源--优雅草科技官方发布



## 技术栈简介


### 技术路线

后端开发语言：PHP+THINKPHP5.00框架

前端开发语言：uniapp（混合开发，非纯原生）

socket推送：goeasy

数据库：mysql5.6

支持客户端：安卓Android端 + 苹果IOS端
 

### 服务器配置 ：

服务器操作系统： Linux Centos 7.x 及以上

软件环境： php 7.0 +nginx+MySQL5.6   


### 第三方服务：

本地图像/视频上传：ffmpeg

短信发送处理：叮咚云/赛邮云/腾讯云 三种可切换

云储存上传：七牛云/阿里云 两种可切换

FTP上传： ftp远程

第三方登陆：微信/微博/QQ 可同时开启

内容安全识别：知道创宇·创宇卫士

IM即时通讯：极光IM / goeasy IM

直播推流： 腾讯云直播推流sdk


除以上配置，未用到任何第三方付费api,无需支付高昂的运营成本

 


## 2022年5.x新版演示截图
![](https://fenfacun.youyacao.com/v5.x.gif)




## 4.2.9老版演示截图

![](https://songshu.youyacao.com/images/video/songshuvideo370.gif)


### 前端截图

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=394e87902cafc7e1c5bad53b2792ce16)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=c2f314059aad380869eee7e7897ce198)


![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=96737a36fea203672460b876553d89b7)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=24afe48b200dfb083ea10baf9e393343)

### 后端截图






#### 仓库关联
前端：[https://gitee.com/youyacao/songshu-video-page](https://gitee.com/youyacao/songshu-video-page)

服务端：[https://gitee.com/youyacao/songshu-video-back](https://gitee.com/youyacao/songshu-video-back)

后台管理：[https://gitee.com/youyacao/songshu-video-admin/](https://gitee.com/youyacao/songshu-video-admin/)

后台管理的前端：[https://gitee.com/youyacao/songshu-video-admin-vue/tree/develop/](https://gitee.com/youyacao/songshu-video-admin-vue/tree/develop/)


注意后端和后台管理前端要用develop分支，其他部分用master分支



#### 说明


松鼠短视频于8月13日完全彻底开源-请遵循授权条款以及开源协议合法使用本产品，



查看协议：[https://songshu.youyacao.com/detail/240.html](https://songshu.youyacao.com/detail/240.html)

系统演示地址：[https://doc.youyacao.com/web/#/22/457](https://doc.youyacao.com/web/#/22/457)

免费gitee下载地址：[https://gitee.com/youyacao/songshu-video-page](https://gitee.com/youyacao/songshu-video-page)

论坛下载地址：[https://bbs.youyacao.com/thread-164-1-1.html](https://bbs.youyacao.com/thread-164-1-1.html)

安装文档：[https://doc.youyacao.com/web/#/4/22](https://doc.youyacao.com/web/#/4/22)

API接口文档：[https://doc.youyacao.com/web/#/17/199](https://doc.youyacao.com/web/#/17/199)

更新日志：[https://doc.youyacao.com/web/#/9/637](https://doc.youyacao.com/web/#/9/637)

官方交流qq群：929353806

由于已经开放至gitee并且接受提交，所以欢迎大家一起来完善打造一个完美的系统，后续更新迭代均发布在doc.youyacao.com信息。


## 介绍
松鼠 短视频系统已经完全开源，请在遵循协议和各条款的情况下合法的使用本产品。

### 前端目录songshu-vide-page
[https://doc.youyacao.com/web/#/4/317](https://doc.youyacao.com/web/#/4/317)

### 服务端目录songshu-video-back
[https://doc.youyacao.com/web/#/4/316](https://doc.youyacao.com/web/#/4/316)
### 后台管理后端部分songshu-video-admin

[https://doc.youyacao.com/web/#/4/640](https://doc.youyacao.com/web/#/4/640)
### 后台管理前端部分songshu-video-admin-vue
[https://doc.youyacao.com/web/#/4/639](https://doc.youyacao.com/web/#/4/639)


## 安装教程

[请查看松鼠的安装教程](https://doc.youyacao.com/web/#/4/22)
[请查看蜻蜓的安装教程](https://doc.youyacao.com/web/#/8/51)

## 使用说明

1.  本项目前端，后端，管理后台所有代码均已开源，测试代码通信接口为https://videofree.youyacao.com,搭建需要需要完整匹配后端。
其次后端我们开放了游客账户可以登录查看部分数据
https://videofreeadmin.youyacao.com
账户 adminkan  密码 123456

2.  请详细查看安装教程，数据库文件是在songshu-video-admin文件里的。
3.  开源代码请在遵循条款和开源许可的请看下合法使用。


## 接口文档说明(最早期松鼠版本是没有写的，此处我们将会持续更新完善):

松鼠短视频： [https://doc.youyacao.com/web/#/17/199](https://doc.youyacao.com/web/#/17/199)



## 技术支持和商业授权

登陆[zhengban.youyacao.com](https://zhengban.youyacao.com)购买商业授权或者技术支持。

## 优雅草软件产品系列目录大全

[点击打开进入浏览优雅草产品目录大全](https://doc.youyacao.com/web/#/8/51 "点击打开进入浏览优雅草产品目录大全")

## 官方客服联系

企业客服QQ:2853810243  交流群：929353806



商务合作/软件开发售前项目咨询：

|  称呼 | 电话  | QQ  | 备注  |
| :------------: | :------------: | :------------: | :------------: |
|高先生   | 13880403799  |   |   |
|陆女士   | 19182077262  |   |   |
|杜先生   | 13708021643  |   |   |
|吴先生   |  15882478524 |   |   |
|李先生   |  18982030165 |   |   |
|黎先生   | 15881024585  |   |   |





纯技术项目咨询：

|  称呼 | 电话  | QQ  | 备注  |
| :------------: | :------------: | :------------: | :------------: |
|  卢先生 | 15198094779  |   |   |
|  吴先生 | 15882478524  |   |   |
|  杜先生| 13708021643  |   |   |
|  张先生 |  18656687900 |   |   |
|   |   |   |   |



# 插一脚·不仔细的用户是看不到滴-本月优雅草科技开始搞活动：

针对名额：前100名用户，50名企业单位，50名个人，以论坛账号盖楼为准，本次活动有效期7月10日开启-8月30日截止，敬请期待，过期不候。

详细步骤：

1，第一步：添加官方QQ群929353806 给我们官方gitee 点个星+fork，选你想要被授权的系统，（不勉强，如果你需要或者想用我们系统才需要，因此你不认可就退回之前看的内容跳过我们的产品即可。）
2，第二步：绑定腾讯云/华为云/（任选其一）与优雅草关联，图个业绩不挣钱但是图绩效考核。
3，第三步：注册论坛账号在盖楼贴 发一条回复支持一下活动即可。

联系客服完成以上三步完成便能和优雅草签约并免费获得商业授权和1年论坛技术支持。



完成以上操作的前100名用户免费获得商业授权（需要与优雅草科技签约）+论坛技术支持一年/单位/个人。




问：我的这个授权和花钱买的授权相同吗？
答：与已购买商业授权相同，唯一不同是每年名额有限。


问：请问包含搭建服务吗？
答：不包含搭建服务，由于搭建是需要人工的，人工成本过高如果免费帮扶搭建是不可以的。


问：请问有技术支持吗？
答：是的，有论坛商业技术支持给指定论坛账号上松鼠/蜻蜓VIP组用户可以发帖求助。

问：请问授权需要签约合同吗？
答：是的，不管是个人和企业都需要签约我们的授权合同，签约采用法大大线上电子签约，需要客户提前注册法大大并且实名电子印章，每份合同签约6元/份，本次活动费用由优雅草官方承担，客户无需承担合同费。



这是优雅草第一期出这样的活动，本年2022年度推出免费商业授权的产品为：松鼠短视频。

另外：

前10名用户将会获得优雅草科技免费搭建服务一次，免费为您完整搭建一次松鼠短视频系统。
