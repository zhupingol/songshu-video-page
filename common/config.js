
// All rights Reserved, Designed By www.youyacao.com 
// @Description:服务器请求地址配置
// @author:成都市一颗优雅草科技有限公司     
// @version V5.x 
// 注意：本前端源码遵循 MulanPSL-2.0开源协议（木兰宽松许可证）本内容仅限于个人参考，禁止用于其他的商业用途
// 需要商业用途或者定制开发等可访问songshu.youyacao.com   企业客服QQ:2853810243  官方客户·技术交流群 929353806交流群 929353806

let url_config = ""

if (process.env.NODE_ENV === 'development') {
	// 开发环境
	url_config = "https://videofree.youyacao.com/"
} else {
	// 生产环境
	url_config = "https://videofree.youyacao.com/"
}

export default url_config
