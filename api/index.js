// All rights Reserved, Designed By www.youyacao.com 
// @Description:api接口文件
// @author:成都市一颗优雅草科技有限公司     
// @version V5.x 
// 注意：本前端源码遵循 MulanPSL-2.0开源协议（木兰宽松许可证）本内容仅限于个人参考，禁止用于其他的商业用途
// 需要商业用途或者定制开发等可访问songshu.youyacao.com   企业客服QQ:2853810243  官方客户·技术交流群 929353806交流群 929353806

import request from '@/common/request.js'

const api = {}

api.getAdd= params => request.globalRequest('/order/add', 'POST', params) 
api.getshopInfo= params => request.globalRequest('shop/info', 'get', params) 
api.getshopType= params => request.globalRequest('shop_type/list', 'POST', params)       
api.getshop = params => request.globalRequest('shop/list', 'GET', params)
api.subjectList = params => request.globalRequest('subject/list', 'GET', params)
api.subjectResult = params => request.globalRequest('subject/result', 'GET', params)
api.safeConfig = params => request.globalRequest('config', 'GET', params)
api.subjectAnswer = data => request.globalRequest('subject/answer', 'POST', data)
// 卡密兑换
api.CardSecretExchange = data => request.globalRequest('cipher/receive', 'POST', data)

// 我喜欢的视频列表
api.LikeVideoList = params => request.globalRequest('skr/list', 'GET', params)

/**
 * 视频相关
 */
const VIDEO = 'video';
// 获取视频列表
api.videoList = params => request.globalRequest(`${VIDEO}/data`, 'GET', params)
//浏览视频
api.videoView = params => request.globalRequest(`${VIDEO}/view`, 'GET', params)
//发布视频
api.videoPublish = params => request.globalRequest(`${VIDEO}/data`, 'POST', params)
//删除视频
api.videoDelete = params => request.globalRequest(`${VIDEO}/delete`, 'POST', params)
// 购买视频
api.videoBuy = params => request.globalRequest(`${VIDEO}/buy`, 'POST', params)
/**
 * 点赞相关
 */
const SKR = 'skr';
//点赞视频
api.videoLike = params => request.globalRequest(`${SKR}/like`, 'GET', params)
/**
 * 点赞评论相关
 */
const SKRCOMMENT = 'skr_comment';
//点赞评论
api.commentLike = params => request.globalRequest(`${SKRCOMMENT}/like`, 'GET', params)

/**
 * 点踩相关
 */
const NEGATIVE = 'negative';
//点踩视频
api.videoNegative = params => request.globalRequest(`${NEGATIVE}/negative`, 'GET', params)
/**
 * 点踩评论相关
 */
const NEGATIVECOMMENT = 'negative_comment';
//点踩评论
api.commentNegative = params => request.globalRequest(`${NEGATIVECOMMENT}/negative`, 'GET', params)

/**
 * 用户相关
 */
const USER = 'user';

//用户登录
api.userLogin = params => request.globalRequest(`${USER}/login`, 'GET', params)
//用户注册
api.userRegister = params => request.globalRequest(`${USER}/register`, 'POST', params)
//获取用户信息
api.userInfo = params => request.globalRequest(`${USER}/userInfo`, 'GET', params)
//验证用户Token信息
api.userAuth = params => request.globalRequest(`${USER}/auth`, 'GET', params)
//用户退出登录
api.userLogout = params => request.globalRequest(`${USER}/logout`, 'GET', params)
//获取其他用户信息
api.userOtherInfo = params => request.globalRequest(`${USER}/otheruserinfo`, 'GET', params)
//获取用户关注列表		follow		FollowList
api.userFollowList = params => request.globalRequest(`${USER}/followList`, 'GET', params)
//获取用户粉丝列表
api.userFansList = params => request.globalRequest(`${USER}/fans`, 'GET', params)
//更新用户资料
api.userUpdate = params => request.globalRequest(`${USER}/update`, 'POST', params)

//获取金币信息
api.userGold = params => request.globalRequest(`${USER}/goldinfo`, 'get', params)
//发送提现验证码
api.getSmsCode = params => request.globalRequest(`${USER}/sms`, 'get', params)
//提交提现申请
api.addWithdraw = params => request.globalRequest(`${USER}/withdraw`, 'post', params)
//提现列表
api.getWithdraw = params => request.globalRequest(`${USER}/withdrawlist`, 'get', params)
//账变记录
api.getChange = params => request.globalRequest(`${USER}/changelist`, 'get', params)
//推广记录
api.getInvite = params => request.globalRequest(`${USER}/invitelist`, 'get', params)
//银行卡列表
api.getCardlist = params => request.globalRequest(`${USER}/cardlist`, 'get', params)
//使用中银行卡
api.getCarduseing = params => request.globalRequest(`${USER}/carduseing`, 'get', params)
//修改银行卡使用状态
api.getCardstatus = params => request.globalRequest(`${USER}/cardstatus`, 'get', params)
//删除银行卡
api.getDeletecard = params => request.globalRequest(`${USER}/deletecard`, 'get', params)
//添加银行卡信息
api.getCardinfo = params => request.globalRequest(`${USER}/cardinfo`, 'get', params)
//添加银行卡信息
api.addCardinfo = params => request.globalRequest(`${USER}/addcard`, 'post', params)
//修改银行卡信息
api.updateCardinfo = params => request.globalRequest(`${USER}/updatecard`, 'post', params)

/**
 * 广告接口
 */
const ADVERT = 'advert';
//发送验证码
api.advertData = params => request.globalRequest(`${ADVERT}/data`, 'GET', params)

/**
 * 评论相关
 */
const COMMENT = 'comment';
//评论列表获取
api.commentList = params => request.globalRequest(`${COMMENT}/list`, 'GET', params)
//发送评论
api.commentSend = params => request.globalRequest(`${COMMENT}/data`, 'POST', params)

/**
 * 搜索相关
 */
const SEARCH = 'search';
//搜索用户
api.searchUser = params => request.globalRequest(`${SEARCH}/user`, 'POST', params)
//搜索视频
api.searchVideo = params => request.globalRequest(`${SEARCH}/video`, 'POST', params)
//获取搜索历史
api.searchHistory = params => request.globalRequest(`${SEARCH}/history`, 'GET', params)
//清除搜索历史
api.searchHistoryClear = params => request.globalRequest(`${SEARCH}/clear`, 'GET', params)

/**
 * 分类相关
 */
const TYPE = 'type';
//获取分类列表
api.typeList = params => request.globalRequest(`${TYPE}/type`, 'GET', params)
api.vipShopList = params => request.globalRequest(`${TYPE}/vipShopList`, 'GET', params)
api.paytypeList = params => request.globalRequest(`${TYPE}/paytypeList`, 'GET', params)
//获取热门搜索
api.typeHot = params => request.globalRequest(`${TYPE}/hot`, 'GET', params)

/**
 * 支付相关
 */
const PAY = 'pay';
api.payment = params => request.globalRequest(`${PAY}/payment`, 'GET', params)

const CODEPAY = 'codePay'
api.codePay = params => request.globalRequest(`${CODEPAY}/index`, 'GET', params)
api.codePayIntegral = params => request.globalRequest(`${CODEPAY}/integral`, 'GET', params)
/**
 * 关注相关
 */
const FOLLOW = 'follow';
//获取分类列表
api.followChange = params => request.globalRequest(`${FOLLOW}/change`, 'POST', params)

/**
 * 关注相关
 */
const COLLECTION = 'collection';
//获取分类列表
api.collectionAdd = params => request.globalRequest(`${COLLECTION}/change`, 'POST', params)

/**
 * API接口
 */
const API = 'api/api';
//发送验证码
api.sendSms = params => request.globalRequest(`${API}/sms`, 'GET', params)
//发送邮件验证码
api.sendMail = params => request.globalRequest(`${API}/mail`, 'GET', params)
//检测更新
api.update = params => request.globalRequest(`${API}/update`, 'POST', params)
//获取系统配置
api.config = params => request.globalRequest(`${API}/config`, 'GET', params)
//获取七牛云Token
api.qiniuToken = params => request.globalRequest(`${API}/getQiNiuToken`, 'GET', params)
export default api


/**
 * 1.3新增
 *  1.图吧功能
 *  2.七牛云上传优化,直接上传到七牛云服务器
 */
/**
 * 图吧接口
 */
const TEXTIMAGE = 'text_image';
//获取图文列表
api.textImageList = params => request.globalRequest(`${TEXTIMAGE}/list`, 'GET', params)
//获取我发布的图文列表
api.textImageUserList = params => request.globalRequest(`${TEXTIMAGE}/userList`, 'GET', params)
//发布图文列表
api.textImageAdd = params => request.globalRequest(`${TEXTIMAGE}/add`, 'POST', params)
//发布图文列表
api.textImageDelete = params => request.globalRequest(`${TEXTIMAGE}/delete`, 'POST', params)
